#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <string.h>
#include <chrono>
#include <iostream>
#include "compaction_classes.h"

static const long int WRITE_BUFFER_SIZE = 64*1024;

void* _open_mmap(const std::string& path,off_t *rsize)
{
  int fd = open(path.c_str(),O_RDWR);
  if(fd < 0)
    {
      std::string msg = "Could not open file: " + path;
      throw Exception(msg);
    }
  
  off_t size = ::lseek(fd,0,SEEK_END);
  if(rsize) *rsize = size;
  void *memory = mmap(NULL,size,PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);
  ::close(fd);
  if(!memory)
    throw Exception("Could not map file");
  return memory;
}

void* _create_zero_mmap(const std::string& path,int size)
{
  int fd = open(path.c_str(),O_RDWR|O_CREAT,0655);
  if(fd < -1)
    {
      std::string msg = "Could not create file: " + path;
      throw Exception(msg);
    }
  ::lseek(fd,size-1,SEEK_SET);
  char buff[] = "\0";
  if(write(fd, buff,1) != 1)
    throw Exception("Cannot write memory file");
  
  void* memory = mmap(NULL,size,PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);
  ::close(fd);
  if(!memory)
    throw Exception("Could not map file");
  memset(memory,0,size);
  return memory;
}

class Lock
{
public:
  Lock(pthread_mutex_t *aLock,bool aLockFlag = true) :
    _lock(aLock),_lockFlag(false)
  {if(aLockFlag) lock();}

  ~Lock() {unLock();}
  inline void lock()
  {
    if(!_lockFlag)
      while(pthread_mutex_lock(_lock)) ;
    _lockFlag = true;
  }
  inline void unLock()
  {
    if(_lockFlag)
      {
	_lockFlag = false;
	pthread_mutex_unlock(_lock);
      }
  }
private:
  pthread_mutex_t *_lock;
  bool   _lockFlag;
};

/*Acquisition::Acquisition():
  m_image_size(-1),
  m_mask_data(NULL),
  m_total_event_data(NULL),
  m_total_intensity_data(NULL),
  m_ev(NULL),
  m_ev_buff(NULL),
  m_ev_index(NULL),
  m_ev_index_buff(NULL),
  m_last_event(0),
  m_thread_id(-1),
  m_stop_flag(false)
{
  pthread_mutex_init(&m_mutex,NULL);
  pthread_cond_init(&m_cond,NULL);
}
*/
Acquisition::Acquisition(const char* file_path, int image_size):
  m_image_size(image_size),
  m_file_base(file_path),
  m_mask_data(NULL),
  m_total_event_data(NULL),
  m_total_intensity_data(NULL),
  m_ev_pos(NULL),
  m_ev_count(NULL),
  m_ev_pos_buff(NULL),
  m_ev_count_buff(NULL),
  m_ev_index(NULL),
  m_ev_index_buff(NULL),
  m_last_event(0),
  m_thread_id(-1),
  m_stop_flag(false),
  m_duration(0)
{
  
  //check if mask  file existe
  std::string mask_file_path = m_file_base + MASK_FILE_EXTENSION;
  if(::access(mask_file_path.c_str(),F_OK))
    m_mask_data = (unsigned char*)_create_zero_mmap(mask_file_path,image_size);
  else
    {
      off_t image_size;
      m_mask_data = (unsigned char*)_open_mmap(mask_file_path,&image_size);
      m_image_size = image_size;
    }
 
  //check if total event  file existe
  std::string total_file_path = m_file_base + TOTALEV_FILE_EXTENSION;
  if(::access(total_file_path.c_str(),F_OK))
    m_total_event_data = (unsigned int*)_create_zero_mmap(total_file_path,image_size*4);
  else
    m_total_event_data = (unsigned int*)_open_mmap(total_file_path,NULL);

  //check if total intensity file existe
  std::string totali_file_path = m_file_base + TOTALI_FILE_EXTENSION;
  if(::access(totali_file_path.c_str(),F_OK))
    m_total_intensity_data = (unsigned int*)_create_zero_mmap(totali_file_path,image_size*4);
  else
    m_total_intensity_data = (unsigned int*)_open_mmap(mask_file_path,NULL);

  std::string ev_file_pos_path = m_file_base + EVENT_POS_FILE_EXTENSION;
  m_ev_pos = fopen(ev_file_pos_path.c_str(),"a");
  if(posix_memalign(&m_ev_pos_buff,4*1024,WRITE_BUFFER_SIZE))
    throw Exception("Cannot allocate file buffer");
  setbuffer(m_ev_pos,(char*)m_ev_pos_buff,WRITE_BUFFER_SIZE);

  std::string ev_file_count_path = m_file_base + EVENT_COUNT_FILE_EXTENSION;
  m_ev_count = fopen(ev_file_count_path.c_str(),"a");
  if(posix_memalign(&m_ev_count_buff,4*1024,WRITE_BUFFER_SIZE))
    throw Exception("Cannot allocate file buffer");
  setbuffer(m_ev_count,(char*)m_ev_count_buff,WRITE_BUFFER_SIZE);

  std::string ev_file_index_path = m_file_base + EVENT_INDEX_EXTENSION;
  m_ev_index = fopen(ev_file_index_path.c_str(),"a+");
  if(posix_memalign(&m_ev_index_buff,4*1024,WRITE_BUFFER_SIZE))
    throw Exception("Cannot allocate file buffer");
  setbuffer(m_ev_index,(char*)m_ev_index_buff,WRITE_BUFFER_SIZE);
  m_last_event = ftell(m_ev_index) / 4;

  //Thread
  pthread_mutex_init(&m_mutex,NULL);
  pthread_cond_init(&m_cond,NULL);
  if(pthread_create(&m_thread_id,NULL,_run,this))
    throw Exception("Cannot create thread");
}

Acquisition::~Acquisition()
{  
  if(m_thread_id > 0)
    {
      Lock lock(&m_mutex);
      m_stop_flag = true;
      pthread_cond_signal(&m_cond);
      lock.unLock();
      void *returnVal;
      while(pthread_join(m_thread_id,&returnVal));
    }
  pthread_mutex_destroy(&m_mutex);
  pthread_cond_destroy(&m_cond);

  if(m_mask_data)
    munmap(m_mask_data,m_image_size);
  if(m_total_event_data)
    munmap(m_total_event_data,m_image_size*4);
  if(m_total_intensity_data)
    munmap(m_total_intensity_data,m_image_size*4);
  if(m_ev_pos)
    fclose(m_ev_pos);
  if(m_ev_pos_buff)
    free(m_ev_pos_buff);
  if(m_ev_count)
    fclose(m_ev_count);
  if(m_ev_count_buff)
    free(m_ev_count_buff);
  if(m_ev_index)
    fclose(m_ev_index);
  if(m_ev_index_buff)
    free(m_ev_index_buff);
  }

void Acquisition::insert(int frame_id,std::vector<unsigned int>& positions,
			 std::vector<unsigned char>& counts)
{
  Lock aLock(&m_mutex);
  if(!m_error.empty())
    throw Exception(m_error);
  if(frame_id < m_last_event)
    throw Exception("Event already added");
  
  m_pending_events[frame_id] = Events(std::move(positions),std::move(counts));
  pthread_cond_signal(&m_cond);
}

void Acquisition::wait_end()
{
  Lock aLock(&m_mutex);
  while(m_error.empty() && !m_pending_events.empty())
    {
      //check missing frames
      auto i= m_pending_events.find(m_last_event);
      if(i == m_pending_events.end())
	throw Exception("Missing frame");
      pthread_cond_wait(&m_cond,&m_mutex);
    }
  if(!m_error.empty())
    throw Exception(m_error);
}

void* Acquisition::_run(void *arg) 
{
  Acquisition* acq = (Acquisition*)arg;
  Lock alock(&acq->m_mutex);
  while(!acq->m_stop_flag)
    {
      auto i = acq->m_pending_events.find(acq->m_last_event);
      if(i != acq->m_pending_events.end())
	{
	  int frame_id = i->first;
	  Events evs = std::move(i->second);
	  acq->m_pending_events.erase(i);
	  acq->m_last_event = frame_id + 1;
	  alock.unLock();
	  // After function call
	  auto start = std::chrono::high_resolution_clock::now();
	  auto ev_pos_i = std::get<0>(evs).begin();
	  auto ev_count_i = std::get<1>(evs).begin();
	  int index = 0;
	  auto positions = new unsigned int[std::get<0>(evs).size()];
	  auto counts = new unsigned char[std::get<1>(evs).size()];
	  auto positions_end = std::get<0>(evs).end();
	  auto event_size = std::get<0>(evs).size();
	  //auto counts_end = std::get<1>(evs).end();
	  for(;
	      ev_pos_i != positions_end ;// && ev_count_i != evs.second.end();
	      ++ev_pos_i,++ev_count_i,++index)

	    {
	      ++acq->m_total_event_data[*ev_pos_i];
	      acq->m_total_intensity_data[*ev_pos_i] += *ev_count_i;
	      positions[index] = *ev_pos_i;
	      counts[index] = *ev_count_i;
	    }
	  
	  size_t nb_ev = ::fwrite(positions,sizeof(unsigned int),event_size,acq->m_ev_pos);
	  if(nb_ev != event_size)
	    {
	      delete positions;
	      delete counts;
	      goto error;
	    }
	  delete positions;
	  
	  nb_ev = ::fwrite(counts,sizeof(unsigned char),event_size,acq->m_ev_count);
	  if(nb_ev != event_size)
	    {
	      delete counts;
	      goto error;
	    }

	  unsigned int total_ev = event_size;
	  size_t t_event_nb = ::fwrite(&total_ev,sizeof(total_ev),1,acq->m_ev_index);
	  if(!t_event_nb)
	    goto error;
	  auto stop = std::chrono::high_resolution_clock::now();
	  auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);

	  acq->m_duration += duration.count();
	  alock.lock();
	  pthread_cond_signal(&acq->m_cond);
	}
      else
	pthread_cond_wait(&acq->m_cond,&acq->m_mutex);
    }
  return NULL;
 error:
  alock.lock();
  acq->m_error = "Could not write file";
  pthread_cond_signal(&acq->m_cond);
  return (void*)-1;
}




// Read class

Read::Read(const char* file_base):
  m_file_base(file_base),
  m_counts(NULL),
  m_times(NULL),
  m_offset(NULL),
  m_stop(false)
{
  pthread_t  thread_id = 0; 
  const int nb_image_to_read = 16;
  unsigned int count_evs[nb_image_to_read];

  std::string total_file_path = m_file_base + TOTALEV_FILE_EXTENSION;
  off_t nb_pixel;
  unsigned int* total_event_data = (unsigned int*)_open_mmap(total_file_path,&nb_pixel);
  nb_pixel /= sizeof(unsigned int);

  std::string ev_file_pos_path = m_file_base + EVENT_POS_FILE_EXTENSION;
  FILE *ev_index_fd = NULL;
  int ev_count_fd = 0;
  int ev_pos_fd;

  std::string ev_file_count_path = m_file_base + EVENT_COUNT_FILE_EXTENSION;
  std::string ev_file_index_path = m_file_base + EVENT_INDEX_EXTENSION;

  pthread_mutex_init(&m_mutex,NULL);
  pthread_cond_init(&m_cond,NULL);

  m_offset = new unsigned long[nb_pixel];
  unsigned long last_offset = 0;
  for(auto index = 0;index < nb_pixel;++index)
    {
      m_offset[index] = last_offset;
      unsigned long cur_offset = total_event_data[index];
      last_offset += cur_offset;
    }
 
  munmap(total_event_data,nb_pixel*sizeof(unsigned int));

  if(posix_memalign((void**)(&m_counts),2*1024*1024,last_offset))
    goto end;
  if(madvise(m_counts,last_offset,MADV_SEQUENTIAL|MADV_WILLNEED|MADV_HUGEPAGE))
    std::cerr << "Cannot get huge page" << std::endl;
  
  if(posix_memalign((void**)(&m_times),2*1024*1024,last_offset * sizeof(int)))
     goto end;
  
  if(madvise(m_times,sizeof(int) * last_offset,MADV_SEQUENTIAL|MADV_WILLNEED|MADV_HUGEPAGE))
    std::cerr << "Cannot get huge page" << std::endl;
    
  m_nb_events = last_offset;

  ev_pos_fd = open(ev_file_pos_path.c_str(),O_RDONLY);
  if(ev_pos_fd < 0)
    goto end;

  ev_count_fd = open(ev_file_count_path.c_str(),O_RDONLY);
  if(!ev_count_fd)
    goto end;

  ev_index_fd = fopen(ev_file_index_path.c_str(),"r");

  if(pthread_create(&thread_id,NULL,_run,this))
    goto end;			// error

  while(true)
    {
      size_t nb_images = fread(&count_evs, sizeof(unsigned int), nb_image_to_read, ev_index_fd);
      if(!nb_images) break;

      int total_event = 0;
      for(size_t i = 0;i< nb_images;++i)
	total_event += count_evs[i];

      void *positions;
      int positions_size = sizeof(unsigned int)*total_event;
      if(posix_memalign(&positions, 4*1024, positions_size))
	goto end;		// set an error

      void *counts;
      int counts_size = sizeof(unsigned char)*total_event;
      if(posix_memalign(&counts, 4*1024, counts_size))
	{
	  free(positions);
	  goto end;		// set an error
	}

      ssize_t positions_size_read = read(ev_pos_fd, positions, positions_size);
      if(positions_size_read != positions_size)
	{
	  free(counts);
	  free(positions);
	  goto end;
	}

      ssize_t counts_size_read = read(ev_count_fd, counts, counts_size);
      if(counts_size_read != counts_size)
	{
	  free(counts);
	  free(positions);
	  goto end;
	}
      _insert(count_evs,nb_images,counts,positions);
    }
  
 end:
  if(thread_id)
    {
      m_stop = true;
      Lock alock(&m_mutex);
      pthread_cond_signal(&m_cond);
      alock.unLock();
      void* returnVal;
      pthread_join(thread_id,&returnVal);
    }
  if(ev_pos_fd)
    close(ev_pos_fd);
  if(ev_count_fd)
    close(ev_count_fd);
  if(ev_index_fd)
    fclose(ev_index_fd);
  delete [] m_offset;
  m_offset = NULL;
}

Read::~Read()
{
  if(m_counts) free(m_counts);
  if(m_times) free(m_times);
  delete [] m_offset;
  pthread_mutex_destroy(&m_mutex);
  pthread_cond_destroy(&m_cond);
}

char* Read::counts() {return m_counts;}
int* Read::times() {return m_times;}
unsigned long Read::nb_events() {return m_nb_events;}
  

void Read::_insert(unsigned int *event_per_image, int nb_images,
		  void* counts,void* positions)
{
  Lock alock(&m_mutex);
  while(m_pending_events.size() > 16)
    pthread_cond_wait(&m_cond,&m_mutex);
  
  m_pending_events.push({event_per_image,nb_images,counts,positions});
  pthread_cond_signal(&m_cond);
}

void* Read::_run(void *arg)
{
  Read *read = (Read*)arg;
  long time = 0;
  Lock alock(&read->m_mutex);
  while(!read->m_pending_events.empty() || !read->m_stop)
    {
      if(read->m_pending_events.empty())
	pthread_cond_wait(&read->m_cond,&read->m_mutex);

      if(!read->m_pending_events.empty())
	{
	  
	  Read::Block &aBlock = read->m_pending_events.front();
	  alock.unLock();
	  
	  long offset = 0;
	  for(auto nb_event: aBlock.m_event_per_image)
	    {
	      for(auto rpos = 0U;rpos < nb_event;++rpos)
		{
		  unsigned char count = aBlock.m_counts[offset + rpos];
		  unsigned int position = aBlock.m_positions[offset + rpos];
		  unsigned long &mem_pos = read->m_offset[position];

		  read->m_counts[mem_pos] = count;
		  read->m_times[mem_pos] = time;
		  ++mem_pos;
		}
	      offset += nb_event;
	      ++time;
	    }
	  alock.lock();
	  read->m_pending_events.pop();
	  pthread_cond_signal(&read->m_cond);
	}
    }
  return NULL;
}

Read::Block::Block(unsigned int* event_per_image,int nb_images,void* counts,void* positions)
{
  m_event_per_image.reserve(nb_images);
  for(auto i = 0;i<nb_images;++i)
    m_event_per_image.push_back(event_per_image[i]);
    
  m_counts = (unsigned char*)counts;
  m_positions = (unsigned int*)positions;
}

Read::Block::Block(Block &&other) :
  m_event_per_image(std::move(other.m_event_per_image)),
  m_counts(other.m_counts),
  m_positions(other.m_positions)
{
  other.m_counts = NULL;
  other.m_positions = NULL;
}

Read::Block::~Block()
{
  if(m_counts) free(m_counts);
  if(m_positions) free(m_positions);
}
