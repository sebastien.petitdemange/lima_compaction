from setuptools.extension import Extension
from setuptools import setup

compaction = Extension("lima_compaction",
                       sources=["compaction.pyx","compaction_classes.cpp"],
                       extra_compile_args=['-pthread'],
                       libraries=[],
                       language="c++")
setup(
    ext_modules = [compaction],
    #extra_compile_args = [], #"-g0"]
    install_requires = ["numpy"],
    setup_requires = ["cython"]
)
