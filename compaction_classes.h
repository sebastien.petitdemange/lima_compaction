#include <stdio.h>
#include <pthread.h>
#include <string>
#include <vector>
#include <map>
#include <queue>

struct Exception
{
  Exception(const std::string& msg) : message(msg) {}
  std::string message;
};


static const char* MASK_FILE_EXTENSION=".mask";
static const char* TOTALEV_FILE_EXTENSION=".total";
static const char* TOTALI_FILE_EXTENSION=".totali";
static const char* EVENT_INDEX_EXTENSION=".evindex";
static const char* EVENT_COUNT_FILE_EXTENSION=".evcount";
static const char* EVENT_POS_FILE_EXTENSION=".evpos";


class Acquisition
{
 public:
  Acquisition(const char* file_base,int image_size);
  ~Acquisition();


  void insert(int frame_id,std::vector<unsigned int>& event_pos,
	      std::vector<unsigned char>& event_counts);
  unsigned char* mask(){return m_mask_data;}
  unsigned long duration() const {return m_duration;}
  void wait_end();
private:
  static void* _run(void *arg);
  
  typedef std::tuple<std::vector<unsigned int>,std::vector<unsigned char>> Events;
  int m_image_size;
  std::string m_file_base;
  unsigned char* m_mask_data;
  unsigned int* m_total_event_data;
  unsigned int* m_total_intensity_data;
  FILE* m_ev_pos;
  FILE* m_ev_count;
  void* m_ev_pos_buff;
  void* m_ev_count_buff;
  FILE* m_ev_index;
  void* m_ev_index_buff;
  long m_last_event;
  pthread_mutex_t m_mutex;
  pthread_cond_t m_cond;
  pthread_t  m_thread_id;
  bool m_stop_flag;
  std::map<unsigned int,Events> m_pending_events;
  std::string m_error;
  unsigned long m_duration;
};

class Read
{
public:
  Read(const char* file_base);
  ~Read();

  char* counts();
  int* times();
  unsigned long nb_events();
  
private:
  struct Block
  {
    Block(unsigned int*,int,void*,void*);
    Block(Block &&);
    ~Block();
    
    std::vector<unsigned int> m_event_per_image;
    unsigned char* m_counts;
    unsigned int* m_positions;
  };
  
  void _insert(unsigned int*,int,void*,void*);
  static void* _run(void *arg);

  std::string m_file_base;
  char* m_counts;
  int* m_times;
  unsigned long m_nb_events;
  unsigned long* m_offset;
  
  pthread_mutex_t m_mutex;
  pthread_cond_t m_cond;
  bool m_stop;
  std::queue<Block> m_pending_events;
};
