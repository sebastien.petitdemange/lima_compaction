# distutils: language = c++
import os
import numpy
cimport numpy
import struct
import cython
from libcpp.vector cimport vector

ctypedef vector[unsigned int] cpp_event_pos_vector
ctypedef vector[unsigned char] cpp_event_count_vector

cdef extern from "compaction_classes.h":
    cdef cppclass Acquisition:
        #Acquisition()
        Acquisition(char* file_path,int image_size)
        void insert(int frameid,cpp_event_pos_vector& pos,cpp_event_count_vector& counts) nogil
        unsigned char* mask() nogil
        unsigned long duration() nogil;
        void wait_end() nogil;
        
    cdef char* MASK_FILE_EXTENSION
    cdef char* TOTALEV_FILE_EXTENSION
    cdef char* TOTALI_FILE_EXTENSION
    cdef char* EVENT_INDEX_EXTENSION
    cdef char* EVENT_POS_FILE_EXTENSION
    cdef char* EVENT_COUNT_FILE_EXTENSION

    cdef cppclass Read:
        Read(char* file_path)
        char* counts()
        int* times()
        unsigned long nb_events()
        
cdef class CompactionAcq:
    """
    Class used by acquisition which produced compaction files
    """
    cdef Acquisition* c_acq
    cdef char* _file_path
    cdef int _threshold
    cdef int _image_size
    
    def __cinit__(self,const char* file_path,unsigned unsigned char[:] mask,
                  int threshold,int image_size):
        self.c_acq = new Acquisition(file_path,image_size)

    def __dealloc__(self):
        del self.c_acq
        
    def __init__(self,file_path,unsigned unsigned char[:] mask,threshold,image_size):
        self._file_path = file_path
        self._image_size = image_size
        self._threshold = threshold
        m = self.c_acq.mask()
        for i,val in enumerate(mask):
            m[i] |= val
            
    @property
    def image_size(self):
        return self._image_size

    def insert(self,frame_id,data):
        """
        Add an extra data to the compaction files.
        frame_id is the number of frame in the acquisition. **frame_id** must starts at 0 to n
        data is a one d array
        """
        self._do_insert_uint(frame_id,data,self._threshold,
                             self.c_acq.mask())
        
    @cython.boundscheck(False) # turn off bounds-checking for entire function
    @cython.wraparound(False)  # turn off negative index wrapping for entire function
    cdef void _do_insert_uint(self,unsigned int frame_id,
                              unsigned unsigned int[:] arr,
                              unsigned int threshold,
                              unsigned char* mask) nogil:
        cdef int arr_shape = arr.shape[0]
        cdef cpp_event_pos_vector positions
        positions.reserve(2048);  # 2 pages
        cdef cpp_event_count_vector counts
        counts.reserve(2048)    # 1 page

        for k in range(arr_shape):
            if arr[k] > 0:
                if arr[k] > threshold:
                    mask[k] = 1
                elif mask[k] < 1:
                    positions.push_back(k)
                    counts.push_back(arr[k])
        
        self.c_acq.insert(frame_id,positions,counts)
    @property
    def duration(self):
        return self.c_acq.duration()
    def wait_end(self):
        return self.c_acq.wait_end()
    
class CompactionRead:
    """
    class for reading file produced by CompactionAcq
    """
    def __init__(self,str file_path):
        self._file_path = file_path.encode()
        self.reload()
        
    def reload(self):
        file_path = self._file_path
        self._mask_data = numpy.memmap(file_path+MASK_FILE_EXTENSION,
                                       dtype='<u1')
        self._total_ev = numpy.memmap(file_path+TOTALEV_FILE_EXTENSION,
                                      dtype='<u4')
        self._total_intensity = numpy.memmap(file_path+TOTALI_FILE_EXTENSION,
                                             dtype='<u4')
        self._ev_index = numpy.memmap(file_path+EVENT_INDEX_EXTENSION,
                                     dtype=numpy.uint32)
        self._ev_pos = numpy.memmap(file_path+EVENT_POS_FILE_EXTENSION,dtype='<u4')
        self._ev_counts = numpy.memmap(file_path+EVENT_COUNT_FILE_EXTENSION,dtype='<u1')

    @property
    def mask(self):
        return self._mask_data
    @property
    def total_event(self):
        return self._total_ev
    @property
    def total_intensity(self):
        return self._total_intensity
    @property
    def ev_index(self):
        return self._ev_index
    @property
    def ev_position(self):
        return self._ev_pos
    @property
    def ev_counts(self):
        return self._ev_counts


cdef class Compaction:
    """
    class for reading and sorting event per pixel
    """
    cdef Read* c_read
    cdef numpy.ndarray _mask_data
    cdef numpy.ndarray _total_ev
    cdef numpy.ndarray _total_intensity

    def __cinit__(self,str file_path):
        self.c_read = new Read(file_path.encode())

    def __dealloc__(self):
        del self.c_read

    def __init__(self,str f_path):
        file_path = f_path.encode()

        self._mask_data = numpy.memmap(file_path+MASK_FILE_EXTENSION,
                                       dtype='<u1')
        self._total_ev = numpy.memmap(file_path+TOTALEV_FILE_EXTENSION,
                                      dtype='<u4')
        self._total_intensity = numpy.memmap(file_path+TOTALI_FILE_EXTENSION,
                                             dtype='<u4')

    @property
    def nb_events(self):
        return self.c_read.nb_events();

    @property
    def counts(self):
        nb_events = self.c_read.nb_events()
        return numpy.frombuffer(memoryview(<char[:nb_events]>self.c_read.counts()),dtype='<i1')
    @property
    def times(self):
        nb_events = self.c_read.nb_events()
        return numpy.frombuffer(memoryview(<int[:nb_events]>self.c_read.times()),dtype='<i4')

    @property
    def mask(self):
        return self._mask_data
    
    @property
    def total_ev(self):
        return self._total_ev

    @property
    def total_intensity(self):
        return self._total_intensity
